package space.danielucas.imagegram;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import space.danielucas.imagegram.view.ContainerActivity;
import space.danielucas.imagegram.view.RegisterActivity;

public class LoginActivity extends AppCompatActivity {
    private String url = "https://www.instagram.com";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void goCreateAccount(View view) {
        Toast.makeText(this, "CreateAccount Run!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    public void login(View view) {
        Intent inten = new Intent(this, ContainerActivity.class);
        startActivity(inten);
    }

    public void openBrowser(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://www.instagram.com/"));
        startActivity(intent);
    }
}
