package space.danielucas.imagegram.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.widget.ImageView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;

import space.danielucas.imagegram.R;

public class PictureDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_detail);
        showToolbar(getIntent().getStringExtra("TITTLE"), true);
        getWindow().setEnterTransition(new Fade());

        ImageView imageView = (ImageView) findViewById(R.id.image_header);
        Glide.with(this)
                .load(getIntent().getStringExtra("IMAGE"))
                .thumbnail(0.1f)
                .into(imageView);
    }

    public void showToolbar(String tittle, boolean upButton) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(tittle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
    }
}
