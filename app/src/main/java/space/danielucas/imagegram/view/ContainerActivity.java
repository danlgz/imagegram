package space.danielucas.imagegram.view;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.FrameLayout;

import space.danielucas.imagegram.R;
import space.danielucas.imagegram.view.fragment.HomeFragment;
import space.danielucas.imagegram.view.fragment.ProfileFragment;
import space.danielucas.imagegram.view.fragment.SearchFragment;

public class ContainerActivity extends AppCompatActivity {
    HomeFragment homeFragment = new HomeFragment();
    SearchFragment searchFragment = new SearchFragment();
    ProfileFragment profileFragment = new ProfileFragment();

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottombar);
        switch (getSupportFragmentManager().findFragmentById(R.id.container).getTag()) {
            case "HOME":
                bottomNavigationView.setSelectedItemId(R.id.home);
                break;
            case "SEARCH":
                bottomNavigationView.setSelectedItemId(R.id.search);
                break;
            case "PROFILE":
                bottomNavigationView.setSelectedItemId(R.id.profile);
                break;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, homeFragment, "HOME")
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottombar);
        bottomNavigationView.setSelectedItemId(R.id.home);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home:
                        if (!(getSupportFragmentManager().findFragmentById(R.id.container).getTag().equals("HOME"))) {
                            getSupportFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.container, homeFragment, "HOME")
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                    .addToBackStack(null)
                                    .commit();
                        }
                        break;
                    case R.id.search:
                        if (!(getSupportFragmentManager().findFragmentById(R.id.container).getTag().equals("SEARCH"))) {
                            getSupportFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.container, searchFragment, "SEARCH")
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                    .addToBackStack(null)
                                    .commit();
                        }
                        break;
                    case R.id.profile:
                        if (!(getSupportFragmentManager().findFragmentById(R.id.container).getTag().equals("PROFILE"))) {
                            getSupportFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.container, profileFragment, "PROFILE")
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                    .addToBackStack(null)
                                    .commit();
                        }
                        break;
                }

                return true;
            }
        });
    }
}
