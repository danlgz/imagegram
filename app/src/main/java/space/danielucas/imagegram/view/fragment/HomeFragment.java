package space.danielucas.imagegram.view.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import space.danielucas.imagegram.R;
import space.danielucas.imagegram.adapter.PictureAdapterRecyclerView;
import space.danielucas.imagegram.model.Card;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        showToolbar(getResources().getString(R.string.menu_home), false, view);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_content_cards);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(linearLayoutManager);

        PictureAdapterRecyclerView pictureAdapterRecyclerView =
                new PictureAdapterRecyclerView(getCards(), R.layout.cardview_picture, getActivity(), "HOME");
        recyclerView.setAdapter(pictureAdapterRecyclerView);

        return view;
    }

    public void showToolbar(String tittle, boolean upButton, View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(tittle);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
    }

    public ArrayList<Card> getCards() {
        ArrayList<Card> array = new ArrayList();
        array.add(new Card("https://static.pexels.com/photos/698539/pexels-photo-698539.jpeg",
                "Monica Ardon", 3, 132));
        array.add(new Card("https://static.pexels.com/photos/714060/pexels-photo-714060.jpeg",
                "Jose Hernandez", 12, 580));
        array.add(new Card("https://static.pexels.com/photos/709802/pexels-photo-709802.jpeg",
                "Anahi Gonzalez", 30, 410));

        return  array;
    }
}
