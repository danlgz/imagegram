package space.danielucas.imagegram.model;

/**
 * Created by danielucas on 12/18/17.
 */

public class Card {

    private String image;
    private String username;
    private int time;
    private int like;

    public Card(String image, String username, int time, int like) {
        this.image = image;
        this.username = username;
        this.time = time;
        this.like = like;
    }

    public Card() {

    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }
}
