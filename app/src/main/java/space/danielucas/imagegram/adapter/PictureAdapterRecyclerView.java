package space.danielucas.imagegram.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import space.danielucas.imagegram.R;
import space.danielucas.imagegram.model.Card;
import space.danielucas.imagegram.view.PictureDetailActivity;

/**
 * Created by danielucas on 12/18/17.
 */

public class PictureAdapterRecyclerView extends RecyclerView.Adapter<PictureAdapterRecyclerView.PictureViewHolder> {
    private ArrayList<Card> cards;
    private int resourse; // El cardView al que se le pasaran datos
    private Activity activity;
    private String action;

    public PictureAdapterRecyclerView(ArrayList<Card> cards, int resourse, Activity activity, String action) {
        this.cards = cards;
        this.resourse = resourse;
        this.activity = activity;
        this.action = action;
    }

    @Override
    public PictureViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resourse, parent, false);
        return new PictureViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PictureViewHolder holder, int position) {
        final Card card = cards.get(position);
        holder.usernameCard.setText(card.getUsername());
        holder.timeCard.setText(String.valueOf(card.getTime()));
        holder.likeNumberCard.setText(String.valueOf(card.getLike()));
        if (this.action == "SEARCH") {
        }
        Glide.with(activity)
                .load(card.getImage())
                .thumbnail(0.1f)
                .into(holder.imageCard);

        holder.imageCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, PictureDetailActivity.class);
                String image = card.getImage();
                String title = card.getUsername();
                intent.putExtra("IMAGE", image);
                intent.putExtra("TITTLE", title);
                Explode explode = new Explode();
                explode.setDuration(1000);
                activity.getWindow().setExitTransition(explode);

                activity.startActivity(intent, ActivityOptionsCompat.
                        makeSceneTransitionAnimation(
                                activity, view, activity.getString(R.string.transition_picture_card)
                        ).toBundle());
            }
        });
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    // ------------------ Clase View Holder --------------------
    public class PictureViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageCard;
        private TextView usernameCard;
        private TextView timeCard;
        private TextView likeNumberCard;

        public PictureViewHolder(View itemView) {
            super(itemView);

            imageCard       = (ImageView) itemView.findViewById(R.id.cardview_image);
            usernameCard    = (TextView) itemView.findViewById(R.id.cardview_username);
            timeCard        = (TextView) itemView.findViewById(R.id.cardview_time);
            likeNumberCard  = (TextView) itemView.findViewById(R.id.like_number);

        }
    }
}
